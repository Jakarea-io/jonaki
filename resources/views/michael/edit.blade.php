@extends('michael.layouts.master')
@section('content')
@section('title', 'Contact')

 <section class="eventforms">
    <div class="rectbg5 rectbg">
      <div class="rectbg5img rectbgimg2"></div>
    </div>

    <div class="container ontopof">
      <form action="{{ url('data/edit',$user->data_id)}}" method="post">
        {{ csrf_field() }}
      <div class="row">
        <div class="col-sm-12 text-center mb-50">
          <h2>Update Your profile</h2>
          <div class="gap-40"></div>
        </div>
      </div>
      
      <div class="row fields">
        <div class="col-sm-6">
          <label>FIRST NAME </label>
          <input class="input" type="text" name="first_name" value="{{$user->first_name}}" placeholder="First Name">
        </div>
        <div class="col-sm-6">
          <label>LAST NAME </label>
          <input class="input" type="text" name="last_name" value="{{$user->last_name}}" placeholder="Last Name">
        </div>
      </div>

      <div class="row fields">
        <div class="col-sm-6">
          <label>EMAIL </label>
          <input class="input" type="text" name="email" value="{{ $user->email}}">
        </div>
        <div class="col-sm-6">
          <label>PHONE</label>
          <input class="input" type="text" value={{$user->phone}} name="phone">
        </div>
      </div>

      <div class="row fields">
        <div class="col-sm-6">
          <label>REGION</label>
          <input class="input" type="text" value={{$user->region}} name="region">
        </div>
        <div class="col-sm-6">
          <label>POSTAL CODE</label>
          <input class="input" type="text" value="{{$user->postal}}" name="postal">
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12 text-center">
          <div class="gap-40"></div>  
          <div class="mb-30"></div>
          <button type="submit" class="submitbtn">Send</button>
          <div class="gap-60"></div>  
        </div>
      </div>

    </form>
    </div>
  </section>
@endsection

  