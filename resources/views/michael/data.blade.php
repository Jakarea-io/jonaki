@extends('michael.layouts.master')
@section('content')
@section('title', 'Contact')

 <section class="eventforms">

    <div class="rectbg5 rectbg">
      <div class="rectbg5img rectbgimg2"></div>
    </div>
    <div class="container ontopof">
    	@if(session('message'))
          <div class="alert alert-success">{{session('message')}}</div>
      @endif

  <table class="table">
    <tr>
      <th>First Name</th>
      <th>First Name</th>
      <th> Phone </th>
      <th> Email </th>
      <th> Region </th>
      <th> Registered </th>
      <th> Actions </th>
    </tr>
    @foreach($data as $datum) 
    <tr>
      <td>{{ $datum->first_name}}</td>
      <td>{{ $datum->last_name}}</td>
      <td>{{ $datum->phone}}</td>
      <td>{{ $datum->email}}</td>
      <td>{{ $datum->region}}</td>
      <td>{{ $datum->created_at->diffForHumans()}}</td>
      <td>
        <a role="button" class="btn btn-small btn-danger" href="{{ url('data/delete',$datum->data_id) }}">Delete</a>
        <a role="button" class="btn btn-small btn-warning" href="{{ url('data/edit',$datum->data_id) }}">Edit</a>
      </td>
    </tr>
@endforeach
    </table>
  </div>
  </section>
@endsection

  