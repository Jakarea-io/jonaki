@extends('michael.layouts.master')
@section('content')
@section('title', 'Upcomming Events')
<section class="business_sec" style="background: url({{ url('assets/michael/images/business_banner.jpg')}});">
  <div class="business_content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h2><span>Business Retreat 2018</span></h2>
          <p>Then Join us to explore: what you don’t know<br/> you don’t know that’s possible.: </p>
          <div class="gap-41"></div>
          <a class="no-arrow btn-color business_btn" href="#">register</a>
        </div>
      </div>
    </div>
  </div>
  <div class="diamond_arrow"></div>
</section>

  <section class="events_sec">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="events_content">
            <h2><span>about event</span></h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consec</p>
            <div class="gap-12"></div>

            <p>tetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna </p>
            <div class="gap-115"></div>
          <h2><span>Speakers and Facilitators</span></h2>
          </div>
        </div>

      </div>
    </div>
  </section>

  <section class="about_sec event_facilitators">
    <div class="container">
      <div class="row">
        <div class="all_situations events_identity clearfix">
          <div class="col-sm-3">
            <div class="hilf_col">
              <div class="hilf_col_inner">
                <img src="{{ url('assets/michael/images/avt_1.jpg')}}" alt="missing">
                <div class="name-designation">
                  <h5>joe doel</h5>
                  <h6>audio innovator</h6>
                </div>
                <div class="hilf_shadow"></div>
                <a href="#"></a>
              </div>
              <span></span>
           </div>
          </div>
          <div class="col-sm-3">
            <div class="hilf_col">
              <div class="hilf_col_inner">
                <img src="{{ url('assets/michael/images/avt_2.jpg')}}" alt="missing">
                <div class="name-designation">
                  <h5>Helena  Williams</h5>
                  <h6>Neural Networker</h6>
                </div>
                <div class="hilf_shadow"></div>
                <a href="#"></a>
              </div>
              <span></span>
           </div>
          </div>
          <div class="col-sm-3">
            <div class="hilf_col">
              <div class="hilf_col_inner">
                <img src="{{ url('assets/michael/images/avt_3.jpg')}}" alt="missing">
                <div class="name-designation">
                  <h5>Tome jones</h5>
                  <h6>Rocket Scientist</h6>
                </div>
                <div class="hilf_shadow"></div>
                <a href="#"></a>
              </div>
              <span></span>
           </div>
          </div>
          <div class="col-sm-3">
            <div class="hilf_col">
              <div class="hilf_col_inner">
                <img src="{{ url('assets/michael/images/avt_4.jpg')}}" alt="missing">
                <div class="name-designation">
                  <h5>kate Stone</h5>
                  <h6>Designer Magician</h6>
                </div>
                <div class="hilf_shadow"></div>
                <a href="#"></a>
              </div>
              <span></span>
           </div>
          </div>                             
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="video-alone-wrap">
            <h2><span>An inside look</span></h2>
            <div class="alvideo clearfix">
                <div class="sliderItemInner">
                  <div class="sliderImg" style="background: url({{ url('assets/michael/images/comfort.jpg')}} no-repeat;"></div>
                  <div class="sliderTriangle"></div>
                  <div class="sliderContent">
                    <div class="sliderContentInner">
                      <h3>Tom Jones</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                    </div>
                  </div>
                  <div class="sliderbtn"></div>
                  <div class="slidervideo"></div>
                </div>              
            </div>
          </div>
        </div>
      </div>

    </div>
  </section>

  <section class="all_works_sec">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="voluanteers">
            <img src="{{ url('assets/michael/images/vol_ico.png')}}" alt="missing">
            <h4><span>VOLUNTEERS</span></h4>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="voluanteers">
            <img src="{{ url('assets/michael/images/speaker_ico.png')}}" alt="missing">
            <h4><span>SPEAKER SUBMISSIONS</span></h4>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="voluanteers">
            <img src="{{ url('assets/michael/images/partners_ico.png')}}" alt="missing">
            <h4><span>partners</span></h4>
          </div>
        </div>                
      </div>
      <div class="row">
        <div class="conference_row">
          <div class="col-sm-4">
            <div class="conference_col">
              <img src="{{ url('assets/michael/images/conf_1.jpg')}}" alt="missing">
              <div class="gap-45"></div>
              <p>Our conferences would not be the success they are without our amazing volunteers. If you are interested in joining us, please <a href="#">tell us a bit about yourself.</a></p>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="conference_col">
              <img src="{{ url('assets/michael/images/conf_2.jpg')}}" alt="missing">
              <div class="gap-45"></div>
              <p>Our conferences would not be the success they are without our amazing volunteers. If you are interested in joining us, please <a href="#">tell us a bit about yourself.</a></p>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="conference_col">
              <img src="{{ url('assets/michael/images/conf_3.jpg')}}" alt="missing">
              <div class="gap-45"></div>
              <p>Our conferences would not be the success they are without our amazing volunteers. If you are interested in joining us, please <a href="#">tell us a bit about yourself.</a></p>
            </div>
          </div>                             
        </div>
      </div>
    </div>
  </section>

 @endsection