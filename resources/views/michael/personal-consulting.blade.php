@extends('michael.layouts.master')
@section('content')
  @section('title', 'Consulte With Us')
  <section class="page-banner cn-vcenter">
    <div class="page-banner-content">
      <div class="container">
        <div class="row">
          <div class="col-sm-offset-2 col-sm-10">
            <div class="banner-cn-wrap">
              <h1 class="title sep-left">“Business Consulting”</h1>
              <div class="font-l font-18">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do<br> eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut<br> enim ad minim veniam, quis nostrud exercitation ullamco</p></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="page-banner-bg" style="background: url({{ url('assets/michael/images/page-banner.jpg')}}) no-repeat; background-size: cover; background-position: center;"></div>
    <div class="diamond_arrow"></div>
  </section><!-- end of .main-content -->

<div class="bg2rect">
  <section class="success-demand section-heading section-normal">
    <div class="container">
      <div class="row row-gap-normal">
        <div class="col-sm-12">
          <h2><span class="maxw-700">Success demands singleness of purpose</span></h2>
            <div class="section-content p-lr-10 font-m">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consec</p>
            <p>tetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
          </div>
        </div>
      </div>

      <div class="row row-gap-normal">
        
        <div class="col-md-15">
          <div class="feature-box text-center">
            <div class="feature-img div-table">
              <div class="feature-img">
                <img src="{{ url('assets/michael/images/increase-work.png')}}">
              </div>
            </div>
            <div class="feature-title div-table">
              <div class="feature-title-inner">
                <h6>Increase Work productivity</h6>
              </div>
            </div>
            <div class="feature-content">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            </div>
          </div>
        </div> <!--end col-->

        <div class="col-md-15">
          <div class="feature-box text-center">
            <div class="feature-img div-table">
              <div class="feature-img">
                <img src="{{ url('assets/michael/images/increase-work.png')}}">
              </div>
            </div>
            <div class="feature-title div-table">
              <div class="feature-title-inner">
                <h6>Increase Work productivity</h6>
              </div>
            </div>
            <div class="feature-content">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            </div>
          </div>
        </div>

        <div class="col-md-15">
          <div class="feature-box text-center">
            <div class="feature-img div-table">
              <div class="feature-img">
                <img src="{{ url('assets/michael/images/increase-work.png')}}">
              </div>
            </div>
            <div class="feature-title div-table">
              <div class="feature-title-inner">
                <h6>Increase Work productivity</h6>
              </div>
            </div>
            <div class="feature-content">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            </div>
          </div>
        </div>

        <div class="col-md-15">
          <div class="feature-box text-center">
            <div class="feature-img div-table">
              <div class="feature-img">
                <img src="{{ url('assets/michael/images/increase-work.png')}}">
              </div>
            </div>
            <div class="feature-title div-table">
              <div class="feature-title-inner">
                <h6>Increase Work productivity</h6>
              </div>
            </div>
            <div class="feature-content">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            </div>
          </div>
        </div>

        <div class="col-md-15">
          <div class="feature-box text-center">
            <div class="feature-img div-table">
              <div class="feature-img">
                <img src="{{ url('assets/michael/images/increase-work.png')}}">
              </div>
            </div>
            <div class="feature-title div-table">
              <div class="feature-title-inner">
                <h6>Increase Work productivity</h6>
              </div>
            </div>
            <div class="feature-content">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            </div>
          </div>
        </div>


      </div>

      <div class="row">
        <div class="col-sm-12 text-center">
          <a class="btnnormal" href="#">Push Me</a>
        </div>
      </div>

    </div>
  </section>

  <section class="testimonials section-normal section-heading">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h2><span>testimonials</span></h2>
          <div class="testimonials-outer">
          <div class="testimonials-wrap">
            <div class="left-qouto"></div>
            <div class="right-qouto"></div>
            <div class="testmonialslider">
              
              <div class="sliderItem clearfix">
                <div class="sliderItemInner">
                  <div class="sliderImg" style="background: url({{ url('assets/michael/images/slideritem.jpg')}}) no-repeat;"></div>
                  <div class="sliderTriangle"></div>
                  <div class="sliderContent">
                    <div class="sliderContentInner">
                      <h3>Tom Jones</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                    </div>
                  </div>
                  <div class="sliderbtn"></div>
                  <div class="slidervideo"></div>
                </div>
              </div>

              <div class="sliderItem clearfix">
                <div class="sliderItemInner">
                  <div class="sliderImg" style="background: url({{ url('assets/michael/images/slideritem.jpg')}}) no-repeat;"></div>
                  <div class="sliderTriangle"></div>
                  <div class="sliderContent">
                    <div class="sliderContentInner">
                      <h3>Tom Jones</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                    </div>
                  </div>
                  <div class="sliderbtn"></div>
                  <div class="slidervideo"></div>
                </div>
              </div>

            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
  
  <section class="promo section-heading background-cover" style="background: url({{url('assets/michael/images/promo-bg2.jpg')}});">
    <div class="promo-content div-table">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h2><span>coaching</span></h2>
            <p>What does an amazing life look like to you?</p>
          </div>
        </div>
      </div>
    </div>
  </section>  

<div class="bg3rect-fill">
  <section class="bears-gifts section-heading section-normal background3">
    <div class="container">
      <div class="row row-gap-normal">
        <div class="col-sm-12">
          <h2><span>The pursuit of mastery bears gifts</span></h2>
            <div class="section-content p-lr-10 font-m">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consec</p>
            <p>tetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
          </div>
        </div>
      </div>

      <div class="row row-gap-normal">
        
        <div class="col-md-15">
          <div class="feature-box text-center">
            <div class="feature-img div-table">
              <div class="feature-img">
                <img src="{{ url('assets/michael/images/increase-work.png')}}">
              </div>
            </div>
            <div class="feature-title div-table">
              <div class="feature-title-inner">
                <h6>Increase Work productivity</h6>
              </div>
            </div>
            <div class="feature-content">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            </div>
          </div>
        </div> <!--end col-->

        <div class="col-md-15">
          <div class="feature-box text-center">
            <div class="feature-img div-table">
              <div class="feature-img">
                <img src="{{ url('assets/michael/images/increase-work.png')}}">
              </div>
            </div>
            <div class="feature-title div-table">
              <div class="feature-title-inner">
                <h6>Increase Work productivity</h6>
              </div>
            </div>
            <div class="feature-content">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            </div>
          </div>
        </div>

        <div class="col-md-15">
          <div class="feature-box text-center">
            <div class="feature-img div-table">
              <div class="feature-img">
                <img src="{{ url('assets/michael/images/increase-work.png')}}">
              </div>
            </div>
            <div class="feature-title div-table">
              <div class="feature-title-inner">
                <h6>Increase Work productivity</h6>
              </div>
            </div>
            <div class="feature-content">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            </div>
          </div>
        </div>

        <div class="col-md-15">
          <div class="feature-box text-center">
            <div class="feature-img div-table">
              <div class="feature-img">
                <img src="{{ url('assets/michael/images/increase-work.png')}}">
              </div>
            </div>
            <div class="feature-title div-table">
              <div class="feature-title-inner">
                <h6>Increase Work productivity</h6>
              </div>
            </div>
            <div class="feature-content">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            </div>
          </div>
        </div>

        <div class="col-md-15">
          <div class="feature-box text-center">
            <div class="feature-img div-table">
              <div class="feature-img">
                <img src="{{ url('assets/michael/images/increase-work.png')}}">
              </div>
            </div>
            <div class="feature-title div-table">
              <div class="feature-title-inner">
                <h6>Increase Work productivity</h6>
              </div>
            </div>
            <div class="feature-content">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            </div>
          </div>
        </div>


      </div>

      <div class="row">
        <div class="col-sm-12 text-center">
          <div class="gap-10px"></div>
          <a class="btnnormal" href="#">Push Me</a>
        </div>
      </div>

    </div>
  </section>

  <section class="testimonialauthor section-heading section-normal">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h2><span>testimonials</span></h2>
            <div class="author-slider-outher">
              <div class="author-slider-wrap">
                <div class="aquoteleft"></div>
                <div class="aquoteright"></div>
                <div class="author-slider">
                  
                  <div class="author-slider-item">
                    <div class="asIteminner">
                      <div class="asItemcn text-center">
                        
                        <div class="asImgwrap">
                          <div class="asImgholder">
                            <div class="asImgit" style="background: url({{ url('assets/michael/images/author.jpg')}}) no-repeat;"></div>
                          </div>
                        </div>

                        <div class="asItemtext">
                          <h3><span>Tom Jones</span></h3>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        </div>

                        <div class="asDate">
                          <p>23.09.17</p>
                        </div>

                      </div>
                    </div>
                  </div>

                  <div class="author-slider-item">
                    <div class="asIteminner">
                      <div class="asItemcn text-center">
                        
                        <div class="asImgwrap">
                          <div class="asImgholder">
                            <div class="asImgit" style="background: url({{ url('assets/michael/images/author.jpg')}}0 no-repeat;"></div>
                          </div>
                        </div>

                        <div class="asItemtext">
                          <h3><span>John Deo</span></h3>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                        </div>

                        <div class="asDate">
                          <p>23.09.17</p>
                        </div>

                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
        </div>        
      </div>
    </div>  
  </section>
</div>

@endsection