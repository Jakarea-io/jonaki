@extends('michael.layouts.master')
  @section('content')
  @section('title', 'Home - This Is Portfolio')
  <section class="lead_section">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="lead_content">
            <img src="{{ url('assets/michael/images/ftr_logo.png') }}" alt="missign">
            <h1>Leading the future</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod<br/> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim <br/> veniam, quis nostrud exercitation ullamco</p>
            <div class="lead_btn clearfix">
              <a href="#">Business Consultancy</a>
              <span>or</span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="diamond_arrow"></div>
  </section><!-- end of .main-content -->

  <section class="mediation_sec">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="mediation_content">
            <h1>Meditation Is a way of life</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod<br/> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim <br/> veniam, quis nostrud exercitation ullamco</p>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="increases">
            <div class="imgwrap">
              <div class="imgholder">
                  <img src="{{ url('assets/michael/images/focus.png') }}" alt="missing">
              </div>
            </div>
            <h5>Increse Focus</h5>
            <p>Lorem ipsum dolor<br/> sit amet, consectetur<br/> adipiscing elit, sed<br/> do eiusmod tempor</p>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="increases">
            <div class="imgwrap">
              <div class="imgholder">
                <img src="{{ url('assets/michael/images/increase-work.png') }}" alt="missing">
              </div>
            </div>
            <h5>Increase Productivity</h5>
            <p>Lorem ipsum dolor<br/> sit amet, consectetur<br/> adipiscing elit, sed<br/> do eiusmod tempor inci-<br/>didunt ut labore et dolore</p>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="increases">
            <div class="imgwrap">
              <div class="imgholder">
                <img src="{{ url('assets/michael/images/mental.png') }}" alt="missing">
              </div>
            </div>
            <h5>Mental Awarness</h5>
            <p>Lorem ipsum dolor sit<br/> amet, consectetur adipi-<br/>scing elit, sed do eiusmod<br/> tempor incididunt ut<br/> labore et dolore magna</p>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="increases">
            <div class="imgwrap">
              <div class="imgholder">
                <img src="{{ url('assets/michael/images/curve.png') }}" alt="missing">
              </div>
            </div>
            <h5>Increase Awarness</h5>
            <p>Lorem ipsum dolor sit<br/> amet, consectetur adip-<br/>iscing elit, sed do eius-<br/>mod tempor incididunt <br/>ut labore et dolore</p>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="push_btn">
            <a  href="#">push me</a>
          </div>
        </div>

      </div>
    </div>
  </section>

  <section class="about_sec">
    <div class="container">
      <div class="row">
        <div class="all_situations clearfix">
          <div class="col-sm-3">
            <div class="hilf_col">
              <div class="hilf_col_inner">
                <img src="{{ url('assets/michael/images/hilf.jpg') }}" alt="missing">
                <h5>hire hilf</h5>
                <div class="hilf_shadow"></div>
                <a href="#"></a>
              </div>
              <span></span>
           </div>
          </div>
          <div class="col-sm-3">
            <div class="hilf_col">
              <div class="hilf_col_inner">
                <img src="{{ url('assets/michael/images/events.png') }}" alt="missing">
                <h5>upcoming events</h5>
                <div class="hilf_shadow"></div>
                <a href="#"></a>
              </div>
              <span></span>
           </div>
          </div>
          <div class="col-sm-3">
            <div class="hilf_col">
              <div class="hilf_col_inner">
                <img src="{{ url('assets/michael/images/life.png') }}" alt="missing">
                <h5>better life</h5>
                <div class="hilf_shadow"></div>
                <a href="#"></a>
              </div>
              <span></span>
           </div>
          </div>
          <div class="col-sm-3">
            <div class="hilf_col">
              <div class="hilf_col_inner">
                <img src="{{ url('assets/michael/images/contact.jpg') }}" alt="missing">
                <h5>contact us</h5>
                <div class="hilf_shadow"></div>
                <a href="#"></a>
              </div>
              <span></span>
           </div>
          </div>                              
        </div>
      </div>
      <div class="row">
        <div class="avater_details clearfix">
          <div class="col-sm-5 col-sm-offset-1 clearfix">
            <div class="avtr_smaile">
              <img src="{{ url('assets/michael/images/avtr_smile.jpg') }}" alt="missing">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="about_cnt">
              <h1>About Michael</h1>
              <h5>Business coach</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="grows_sec">
    <div class="container">
      <div class="grows_content">
        <div class="row">
          <div class="col-sm-12">
            <h1>Let’s Grow<br/> together</h1>
          </div>
          <div class="col-sm-6">
            <div class="business_cnt">
              <div class="business_cnt_inner">
               <img src="{{ url('assets/michael/images/hand_shak_logo.png') }}" alt="missing">
                <h2>Business Consultency</h2>
                <div class="business_praghraph">
                  <p>Is you're business as efficient<br/> as you want it to be. As time<br/> and technology pregress,<br/> having a competitive edge is<br/> a must in the business world.</p>
                </div>
                <div class="learn_btn">
                  <a href="#">learn more</a>
                </div>                
              </div>
              <div class="business_opacity"></div>    
            </div>
          </div>
          <div class="col-sm-6">
            <div class="business_cnt busins_last_cont">
              <div class="business_cnt_inner">
               <img src="{{ url('assets/michael/images/bulb.png') }}" alt="missing">
                <h2>Business Consultency</h2>
                <div class="business_praghraph">
                  <p>Are you living the 10 life you want<br/> to live? Are you maximizing the<br/> experiences in you're live. I know i<br/> wasn't a couple of year's ago.<br/> That's why i want to help you not<br/> go through that.</p>
                </div>
                <div class="learn_btn">
                  <a href="#">learn more</a>
                </div>                
              </div>
              <div class="business_opacity"></div>    
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<section class="promo section-heading background-cover" style="background: url(images/promo-bg2.jpg);">
  <div class="promo-content div-table">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h2><span>Get the Insight Scoop</span></h2>
          <p>Living a productive and effective life take's a lot of work. It take's<br> reading, learning, and experiencing. Joing my blogs<br> and get an insight scoop into how we make it all happen.</p>
          <div class="gap-45"></div>
          <a class="no-arrow btn-color" href="#">Sign Up</a>
        </div>
      </div>
    </div>
  </div>
  <div class="diamond_arrow"></div>
</section>

<section class="micel_sec">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1>The Michael   blog</h1>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-4">
        <div class="cat_item">
          <div class="cat_img_box">
            <img src="{{ url('assets/michael/images/cat_1.jpg') }}" alt="missing">
            <a href="#"  class="box-hover" style="background-color: rgba(68,176,99,.8);">
              <span>
                <img src="{{ url('assets/michael/images/box-hover-icon-1.png') }}">
              </span>
            </a>
          </div>
          <div class="cat_des">
            <span class="cat-title" style="color: #30ab56;">Category 1</span>
            <h3>Lorem ipsum dolor sit</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <a class="read_more" href="#">read more</a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="cat_item">
          <div class="cat_img_box">
            <img src="{{ url('assets/michael/images/cat_2.jpg') }}" alt="missing">
            <a href="#"  class="box-hover" style="background-color: rgba(1255,150,38,.8);">
              <span>
                <img src="{{ url('assets/michael/images/box-hover-icon-1.png') }}">
              </span>
            </a>
          </div>
          <div class="cat_des">
            <span class="cat-title" style="color: #ff9626;">Category 2</span>
            <h3>Lorem ipsum dolor sit</h3>
            <p>Lorem ipsum dolor sit amet, consectetur </p>
            <a class="read_more" href="#">read more</a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="cat_item">
          <div class="cat_img_box">
            <img src="{{ url('assets/michael/images/cat_2.jpg') }}" alt="missing">
            <a href="#" class="box-hover" style="background-color: rgba(156,80,191,.8);">
              <span>
                <img src="{{ url('assets/michael/images/box-hover-icon-1.png') }}">
              </span>
            </a>
          </div>
          <div class="cat_des">
            <span class="cat-title" style="color: #9c50bf;">Category 2</span>
            <h3>Lorem ipsum dolor sit</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            <a class="read_more" href="#">read more</a>
          </div>
        </div>
      </div>  

    </div>
  </div>
</section>
@endsection