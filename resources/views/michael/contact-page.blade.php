@extends('michael.layouts.master')
@section('content')
@section('title', 'Contact')

 <section class="eventforms">

    <div class="rectbg5 rectbg">
      <div class="rectbg5img rectbgimg2"></div>
    </div>

    <div class="container ontopof">

       @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif


      <form action="{{ url('data-store')}}" method="post">
        {{ csrf_field() }}
      <div class="row">
        <div class="col-sm-12 text-center mb-50">
          <h2>About you</h2>
          <div class="gap-40"></div>
        </div>
      </div>
      
      <div class="row fields">
        <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">  
          <div class="col-sm-6">
            <label>FIRST NAME </label>
            <input class="input form-control" type="text" name="first_name" placeholder="First Name" value={{ old('first_name') }}>
            <small class="help-block">{{ $errors->first('first_name')}}</small>
          </div>
        </div>
        <div class="col-sm-6">
          <label>LAST NAME </label>
          <input class="input form-control" type="text" name="last_name" placeholder="Last Name" value="{{ old('last_name') }}">
          <small class="help-block">{{ $errors->first('first_name')}}</small>
        </div>
      </div>
        <input type="checkbox" name="ck" value="1" @if(old('ck')) {{'checked'}} @endif>







      <div class="row fields">
        <div class="form-group {{$errors->has('email')? 'has-error':''}}">  
          <div class="col-sm-6">
            <label>EMAIL </label>
            <input class="input form-control" type="text" name="email" value=" {{old('email')}} ">
            <span class="help-block">{{$errors->first('email')}}</span>
          </div>
      </div>
      
        <div class="form-group {{ $errors->has('phone')?'has-error':''}}">  
          <div class="col-sm-6">
            <label>PHONE</label>
            <input class="input form-control" type="text" name="phone" value="{{ old('phone')}}">
            <span class="help-block">{{$errors->first('phone')}}</span>
          </div>
        </div>
      </div>















      <div class="row fields">
        <div class="col-sm-6">
          <label>REGION</label>
          <input class="input form-control" type="text" name="region">
        </div>
        <div class="col-sm-6">
          <label>POSTAL CODE</label>
          <input class="input form-control" type="text" name="postal">
        </div>
      </div>

      <div class="row fields">
        <div class="col-sm-6">
          <label>CITY</label>
          <div class="select_box">
            <select class="input form-control"><option>A</option><option>A</option><option>A</option><option>A</option></select>
          </div>
        </div>
        <div class="col-sm-6">
          <label>COUNTRY</label>
          <div class="select_box">
            <select class="input form-control"><option>A</option><option>A</option><option>A</option><option>A</option></select>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12 text-center">
          <div class="gap-40"></div>  
          <div class="mb-30"></div>
          <button type="submit" class="submitbtn">Send</button>
          <div class="gap-60"></div>  
        </div>
      </div>

    </form>
    </div>
  </section>
@endsection

  