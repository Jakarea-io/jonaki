@extends('michael.layouts.master')
@section('content')
@section('title', 'Our Blog')
	<div class="blog">  

  <section class="lead_section" style="background: url({{ url('assets/michael/images/blog-banner.jpg')}}; background-size: cover; background-position: center center; background-repeat: no-repeat; min-height: 662px;">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="banner_content">
            <span class="bnr-cat-title">Category 1</span>
            <h1>“LIFE IS HAPPENING FOR ME”</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
            <a class="bnr_transparent_btn" href="#">Read more</a>
          </div>
        </div>
      </div>
    </div>

  </section><!-- end of .main-content -->

<section class="micel_sec">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <ul id="" class="filter_menu clearfix">
          <li><a class="button is-checked" href="#" data-filter="*">more recent</a></li>
          <li><a class="button" href="#" data-filter=".group_a">cATEGORY 1</a></li>
          <li><a class="button" href="#" data-filter=".group_b">cATEGORY 2</a></li>
          <li><a class="button" href="#" data-filter=".group_c">cATEGORY 3</a></li>
          <li><a class="button" href="#" data-filter=".group_b">cATEGORY 4</a></li>
          <li><a class="button" href="#" data-filter=".group_c">cATEGORY 5</a></li>
        </ul>
      </div>
    </div>

    <div class="row" id="isotop" class="isotop">

      <div class="col-sm-4 group_a iso_item">
        <div class="cat_item">
          <div class="cat_img_box">
            <img src="{{ url('assets/michael/images/cat_1.jpg')}}" alt="missing">
            <a href="#"  class="box-hover" style="background-color: rgba(68,176,99,.8);">
              <span>
                <img src="{{ url('assets/michael/images/box-hover-icon-1.png')}}">
              </span>
            </a>
          </div>
          <div class="cat_des">
            <span class="cat-title" style="color: #30ab56;">Category 1</span>
            <h3>Lorem ipsum dolor sit</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <a class="read_more" href="#">read more</a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 group_b iso_item">
        <div class="cat_item">
          <div class="cat_img_box">
            <img src="{{ url('assets/michael/images/cat_2.jpg')}}" alt="missing">
            <a href="#"  class="box-hover" style="background-color: rgba(1255,150,38,.8);">
              <span>
                <img src="{{ url('assets/michael/images/box-hover-icon-1.png')}}">
              </span>
            </a>
          </div>
          <div class="cat_des">
            <span class="cat-title" style="color: #ff9626;">Category 2</span>
            <h3>Lorem ipsum dolor sit</h3>
            <p>Lorem ipsum dolor sit amet, consectetur </p>
            <a class="read_more" href="#">read more</a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 group_c iso_item">
        <div class="cat_item">
          <div class="cat_img_box">
            <img src="{{ url('assets/michael/images/cat_6.jpg')}}" alt="missing">
            <a href="#" class="box-hover" style="background-color: rgba(156,80,191,.8);">
              <span>
                <img src="{{ url('assets/michael/images/box-hover-icon-1.png')}}">
              </span>
            </a>
          </div>
          <div class="cat_des">
            <span class="cat-title" style="color: #9c50bf;">Category 2</span>
            <h3>Lorem ipsum dolor sit</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            <a class="read_more" href="#">read more</a>
          </div>
        </div>
      </div>  


      <div class="col-sm-4 group_a iso_item">
        <div class="cat_item">
          <div class="cat_img_box">
            <img src="{{ url('assets/michael/images/cat_4.jpg')}}" alt="missing">
            <a href="#"  class="box-hover" style="background-color: rgba(156,80,191,.8);">
              <span>
                <img src="{{ url('assets/michael/images/box-hover-icon-1.png')}}">
              </span>
            </a>
          </div>
          <div class="cat_des">
            <span class="cat-title" style="color: #9c50bf;">Category 1</span>
            <h3>Lorem ipsum dolor sit</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <a class="read_more" href="#">read more</a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 group_b iso_item">
        <div class="cat_item">
          <div class="cat_img_box">
            <img src="{{ url('assets/michael/images/cat_5.jpg')}}" alt="missing">
            <a href="#"  class="box-hover" style="background-color: rgba(255,38,38,.8);">
              <span>
                <img src="{{ url('assets/michael/images/box-hover-icon-1.png')}}">
              </span>
            </a>
          </div>
          <div class="cat_des">
            <span class="cat-title" style="color: #ff2626;">Category 2</span>
            <h3>Lorem ipsum dolor sit</h3>
            <p>Lorem ipsum dolor sit amet, consectetur </p>
            <a class="read_more" href="#">read more</a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 group_c iso_item">
        <div class="cat_item">
          <div class="cat_img_box">
            <img src="{{ url('assets/michael/images/cat_6.jpg')}}" alt="missing">
            <a href="#" class="box-hover" style="background-color: rgba(79,203,255,.8);">
              <span>
                <img src="{{ url('assets/michael/images/box-hover-icon-1.png')}}">
              </span>
            </a>
          </div>
          <div class="cat_des">
            <span class="cat-title" style="color: #4fcbff;">Category 2</span>
            <h3>Lorem ipsum dolor sit</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
            <a class="read_more" href="#">read more</a>
          </div>
        </div>
      </div>  

    </div>
  </div>
</section>

</div>
@endsection