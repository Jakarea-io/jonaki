@extends('michael.layouts.master')
@section('content')
  @section('title', 'Our Keynote')
  <section class="page-banner background-cover" style="background: url({{ url('assets/michael/images/keynote.jpg')}}) no-repeat;">
    <div class="keynote-banner-cn">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="page-banner-cn text-center maxw-700 hcenter">
              <h1 class="title"><span>"Hire Hilf"</span></h1>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <br>incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoLorem ipsum dolor sit amet, consectetur adipiscing elit</p>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </section><!-- end of .main-content -->

<div class="bgfullpage">

  <section class="video-alone videotop">
    
    <div class="rectbg1 rectbg">
      <div class="rectbg1img rectbgimg1"></div>
    </div>

    <div class="rectbg2 rectbg">
      <div class="rectbg2img rectbgimg1"></div>
    </div>

    <div class="container ontopof">
      <div class="row">
        <div class="col-sm-12">
          <div class="video-alone-wrap">
            <div class="alvideo clearfix">
                <div class="sliderItemInner">
                  <div class="sliderImg" style="background: url({{ url('assets/michael/images/slideritem.jpg')}}) no-repeat;"></div>
                  <div class="sliderTriangle"></div>
                  <div class="sliderContent">
                    <div class="sliderContentInner">
                      <h3>Tom Jones</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                    </div>
                  </div>
                  <div class="sliderbtn"></div>
                  <div class="slidervideo"></div>
                </div>              
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="video-alone-cn">
            <div class="alvideoform">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
              <div class="gap-30"></div>  
              <div class="mb-40"></div>  
              <div class="row mb-30">
                <div class="col-sm-6">
                  <input class="input" type="text" name="name" placeholder="Name">
                </div>
                <div class="col-sm-6">
                  <input class="input" type="email" name="email" placeholder="Email">
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <textarea class="textarea" placeholder="Message"></textarea>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12 text-center">
                  <div class="gap-30"></div>  
                  <div class="mb-30"></div>
                  <button type="submit" class="submitbtn">Send</button>
                  <div class="gap-60"></div>  
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

    </div>
  </section>

  <section class="fee-quote section-heading section-normal">

    <div class="rectbg3 rectbg">
      <div class="rectbg3img rectbgimg2"></div>
    </div>
    <div class="rectbg4 rectbg">
      <div class="rectbg4img rectbgimg3"></div>
    </div>

    <div class="container ontopof">
      <div class="row">
        <div class="col-sm-12 text-center">
          <div class="gap-20"></div>
          <h2><span>  Request a free quote  </span></h2>
          <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, <br>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br> quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
        </div>
        <div class="col-sm-12">
          <div class="review">
            <div class="review-in clearfix">
              <div class="reviewimg">
                <img src="{{ url('assets/michael/images/review.jpg')}}">
              </div>
              <div class="reviewcn">
                <div class="review-title">
                  <h3>Matt Walker - Adventurer // Author // Leadership  </h3>
                </div>
                <div class="review-text">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                </div>
                <div class="rating"><img src="{{ url('assets/michael/images/stars.png')}}"> <span>10 REVIEWS</span></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="eventforms">

    <div class="rectbg5 rectbg">
      <div class="rectbg5img rectbgimg2"></div>
    </div>

    <div class="container ontopof">
      <div class="row">
        <div class="col-sm-12 text-center mb-50">
          <h2>  About your event  </h2>
          <div class="gap-40"></div>
        </div>
      </div>
      
      <div class="row fields">
        <div class="col-sm-6">
          <label>I'M PLANNING A </label>
          <div class="select_box">
            <select class="input"><option>A</option><option>A</option><option>A</option><option>A</option></select>
          </div>
        </div>
        <div class="col-sm-6">
          <label>DATE OF EVENT  </label>
          <input class="input" type="date" name="">
        </div>
      </div>

      <div class="row fields">
        <div class="col-sm-6">
          <label>I'M PLANNING A </label>
          <div class="select_box">
            <select class="input"><option>A</option><option>A</option><option>A</option><option>A</option></select>
          </div>
        </div>
        <div class="col-sm-6">
          <label>DATE OF EVENT  </label>
          <input class="input" type="date" name="">
        </div>
      </div>

      <div class="row fields">
        <div class="col-sm-6">
          <label>I'M PLANNING A </label>
          <div class="select_box">
            <select class="input"><option>A</option><option>A</option><option>A</option><option>A</option></select>
          </div>
        </div>
        <div class="col-sm-6">
          <label>DATE OF EVENT  </label>
            <input class="input" type="date" name="">
        </div>
      </div>

      <div class="row fields">
        <div class="col-sm-6">
          <label>I'M PLANNING A </label>
          <div class="select_box">
            <select class="input"><option>A</option><option>A</option><option>A</option><option>A</option></select>
          </div>
        </div>
        <div class="col-sm-6">
          <label>DATE OF EVENT  </label>
          <textarea class="textarea" placeholder="Message"></textarea>
        </div>
      </div>
      
      <div class="row">
        <div class="col-sm-12">
          <div class="gap-40"></div>
          <div class="gap-30"></div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12 text-center mb-50">
          <h2>    About you  </h2>
          <div class="gap-40"></div>
        </div>
      </div>
      
      <div class="row fields">
        <div class="col-sm-6">
          <label>FIRST NAME </label>
          <input class="input" type="text" name="" placeholder="First Name">
        </div>
        <div class="col-sm-6">
          <label>LAST NAME </label>
          <input class="input" type="text" name="" placeholder="Last Name">
        </div>
      </div>

      <div class="row fields">
        <div class="col-sm-6">
          <label>FIRST NAME </label>
          <input class="input" type="text" name="">
        </div>
        <div class="col-sm-6">
          <label>LAST NAME </label>
          <input class="input" type="text" name="">
        </div>
      </div>
      <div class="row fields">
        <div class="col-sm-6">
          <label>FIRST NAME </label>
          <input class="input" type="text" name="">
        </div>
        <div class="col-sm-6">
          <label>LAST NAME </label>
          <input class="input" type="text" name="">
        </div>
      </div>

      <div class="row fields">
        <div class="col-sm-6">
          <label>I'M A </label>
          <div class="select_box">
            <select class="input"><option>A</option><option>A</option><option>A</option><option>A</option></select>
          </div>
        </div>
        <div class="col-sm-6">
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12 text-center font-deny">
          <div class="gap-30"></div>  
          <p>BY REQUESTING A FREE QUOTE, YOU AGREE TO THE TERMS OF GIGMASTERS <a href="#"> USER AGREEMENT</a> AND <a href="#">PRIVACY POLICY.</a></p>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12 text-center">
          <div class="gap-40"></div>  
          <div class="mb-30"></div>
          <button type="submit" class="submitbtn">Send</button>
          <div class="gap-60"></div>  
        </div>
      </div>


    </div>
  </section>

</div>

 @endsection