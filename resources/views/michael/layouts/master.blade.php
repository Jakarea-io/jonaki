<!DOCTYPE html>
<html>
<head>
  <title>@yield('title') Page</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" href="{{ url('assets/michael/css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/michael/fonts/font-awesome/font-awesome.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/michael/css/animate.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/michael/owl.carousel.1/owl.carousel.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/michael/owl.carousel.1/owl.theme.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/michael/owl.carousel.1/owl.transitions.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/michael/nivo.slider/nivo.slider.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/michael/nivo.slider/themes/default/default.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('assets/michael/slick.slider/slick-theme.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/michael/slick.slider/slick.css') }}">

  <link rel="stylesheet" type="text/css" href="{{ url('assets/michael/fonts/custom-fonts.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/michael/css/common.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/michael/css/per-consulting.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/michael/css/keynote.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/michael/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/michael/css/responsive.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/michael/css/events.css') }}">  

  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->  

</head>
<body>
  
  <header class="header">
    <div class="container">
      <div class="row">
        <div class="col-sm-9">
          <div class="hdr_left_content">
            <div class="hdr_logo">
              <h1>
                <a href="#">
                  <img src="{{ url('assets/michael/images/hdr_logo.jpg') }}" alt="Logo">
                </a>
              </h1>
            </div>            
            <nav class="hdr_nav">
              <ul class="clearfix">
                <li><a href="{{ url('/')}}">Home</a></li>
                <li><a href="{{ url('/blog')}}">Blog</a></li>
                <li><a href="{{ url('/event')}}">Event's</a></li>
                <li><a href="{{ url('/keynote')}}">Keynote</a></li>
                <li><a href="{{ url('/consulting')}}">Consult With Me</a></li>
              </ul>
            </nav>            
          </div>
        </div>
        <div class="col-sm-3">
          <div class="hdr_contact clearfix">
            <a class="phn" tel:'1-800-888-4040' href="#"><i class="fa fa-phone" aria-hidden="true"></i><span>1-800-</span><span>888-4040</span></a>
            <a class="linkedin" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            <a class="fb" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a class="inst" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
          </div>
        </div>        
      </div>
    </div>

  </header><!-- end of header -->
  
  @yield('content')

<footer class="footer">
  <div class="container">
    <div class="row frt_top">
      <div class="col-sm-8">
        <div class="ftr_logo">
          <img src="{{ url('assets/michael/images/ftr_logo.png') }}" alt="missing">
        </div>
      </div>
      <div class="col-sm-4">
        <div class="ftr_scl_icons clearfix">
            <span>Connect with Michael</span>
            <a class="ftr_linkdn" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            <a class="ftr_fb" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a class="ftr_inst" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
        </div>
      </div>
    </div>   

    <div class="row">
      <div class="col-sm-8">
        <div class="copyright">
          <span> © 2017   All rights reserved.</span>
        </div>
      </div>
      <div class="col-sm-4">
        <ul class="trms_and_policy">
          <li><a href="#">Terms</a></li>
          <li><a href="#">Return Policy</a></li>
        </ul>
      </div>
    </div>
  </div>
</footer><!-- end of footer -->

<script src="https://code.jquery.com/jquery-3.0.0.js"></script>
<script src="https://code.jquery.com/jquery-migrate-3.0.0.js"></script>
<script src="{{ url('assets/michael/js/bootstrap.min.js') }}"></script>
<script src="{{ url('assets/michael/owl.carousel.1/owl.carousel.min.js') }}"></script>
<script src="{{ url('assets/michael/nivo.slider/nivo.slider.js') }}"></script>
<script src="{{ url('assets/michael/slick.slider/slick.js') }}"></script>
<script src="{{ url('assets/michael/js/wow.min.js') }}"></script>

<script src="{{ url('assets/michael/js/main.js') }}"></script>
</body>
</html>