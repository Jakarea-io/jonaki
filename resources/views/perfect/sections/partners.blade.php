		<section id="partners" class="section-padding">
			<div class="container">
				<div class="row wow animated fadeInDown">
					<div class="col-md-12 text-center hero-title">
						<i class="fa fa-bomb"></i>
						<h2 class="page-title">Our partners</h2>
						<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI. Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. </p>					
					</div><!--hero title end-->			
				</div>
				<div class="row wow animated fadeInUp">
					<div class="col-md-12">
						<div class="partner-list">
							
							<div class="single-partner">
								<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/partners/01.png')}}" alt="" />
							</div>
							<div class="single-partner">
								<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/partners/02.png')}}" alt="" />
							</div>
							<div class="single-partner">
								<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/partners/03.png')}}" alt="" />
							</div>
							<div class="single-partner">
								<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/partners/04.png')}}" alt="" />
							</div>
							<div class="single-partner">
								<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/partners/05.png')}}" alt="" />
							</div>
							<div class="single-partner">
								<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/partners/06.png')}}" alt="" />
							</div>
							<div class="single-partner">
								<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/partners/07.png')}}" alt="" />
							</div>
							<div class="single-partner">
								<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/partners/08.png')}}" alt="" />
							</div>
							<div class="single-partner">
								<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/partners/09.png')}}" alt="" />
							</div>
							
						</div><!--partner list end-->
					</div>
				</div>
			</div>
		</section>