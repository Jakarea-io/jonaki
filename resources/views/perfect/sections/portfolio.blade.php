<section id="portfolio" class="section-padding">
            <div class="container">
				<div class="row wow animated fadeInDown">
					<div class="col-md-12 text-center hero-title">
						<i class="fa fa-plane"></i>
						<h2 class="page-title">Our portfolio</h2>
						<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI. Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. </p>					
					</div><!--hero title end-->	
				</div>
			
                <div class="row">
                    <div class="col-md-12 wow animated fadeInUp">
                        <ul class="portfolio-filters text-center">
                            <li class="filter active" data-filter="*">all</li>
                            <li class="filter" data-filter=".design">design</li>
                            <li class="filter" data-filter=".html">HTML5</li>
                            <li class="filter" data-filter=".wordpress">Wordpress</li>
                            <li class="filter" data-filter=".seo">Seo</li>
                        </ul><!--.portfolio-filter nav-->

                        <div id="portfolio_list" class="row">
						
							<div class="col-sm-4 col-xs-12 pitem design">
								<div class="portfolio_item">
									<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/portfolio/thumb/01.jpg')}}" alt="" />
									<div class="portfolio-detail">
										<div class="portfolio-overlay"></div>
										<a href="{{url('assets/perfect/img/demo-images/portfolio/full/01.jpg')}}" data-litebox-group="myGallery" class="litebox"></a>
									</div>
								</div>
							</div><!--portfolio col end-->
						
							<div class="col-sm-4 col-xs-12 pitem html">
								<div class="portfolio_item">
									<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/portfolio/thumb/02.jpg')}}" alt="" />
									<div class="portfolio-detail">
										<div class="portfolio-overlay"></div>
										<a href="{{url('assets/perfect/img/demo-images/portfolio/full/02.jpg')}}" data-litebox-group="myGallery" class="litebox"></a>
									</div>
								</div>
							</div><!--portfolio col end-->
						
							<div class="col-sm-4 col-xs-12 pitem design">
								<div class="portfolio_item">
									<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/portfolio/thumb/03.jpg')}}" alt="" />
									<div class="portfolio-detail">
										<div class="portfolio-overlay"></div>
										<a href="{{url('assets/perfect/img/demo-images/portfolio/full/03.jpg')}}" data-litebox-group="myGallery" class="litebox"></a>
									</div>
								</div>
							</div><!--portfolio col end-->
						
							<div class="col-sm-4 col-xs-12 pitem html">
								<div class="portfolio_item">
									<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/portfolio/thumb/04.jpg')}}" alt="" />
									<div class="portfolio-detail">
										<div class="portfolio-overlay"></div>
										<a href="{{url('assets/perfect/img/demo-images/portfolio/full/04.jpg')}}" data-litebox-group="myGallery" class="litebox"></a>
									</div>
								</div>
							</div><!--portfolio col end-->
						
							<div class="col-sm-4 col-xs-12 pitem design">
								<div class="portfolio_item">
									<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/portfolio/thumb/05.jpg')}}" alt="" />
									<div class="portfolio-detail">
										<div class="portfolio-overlay"></div>
										<a href="{{url('assets/perfect/img/demo-images/portfolio/full/05.jpg')}}" data-litebox-group="myGallery" class="litebox"></a>
									</div>
								</div>
							</div><!--portfolio col end-->
						
							<div class="col-sm-4 col-xs-12 pitem design">
								<div class="portfolio_item">
									<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/portfolio/thumb/06.jpg')}}" alt="" />
									<div class="portfolio-detail">
										<div class="portfolio-overlay"></div>
										<a href="{{url('assets/perfect/img/demo-images/portfolio/full/06.jpg')}}" data-litebox-group="myGallery" class="litebox"></a>
									</div>
								</div>
							</div><!--portfolio col end-->
						
							<div class="col-sm-4 col-xs-12 pitem seo">
								<div class="portfolio_item">
									<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/portfolio/thumb/07.jpg')}}" alt="" />
									<div class="portfolio-detail">
										<div class="portfolio-overlay"></div>
										<a href="{{url('assets/perfect/img/demo-images/portfolio/full/07.jpg')}}" data-litebox-group="myGallery" class="litebox"></a>
									</div>
								</div>
							</div><!--portfolio col end-->
						
							<div class="col-sm-4 col-xs-12 pitem wordpress">
								<div class="portfolio_item">
									<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/portfolio/thumb/08.jpg')}}" alt="" />
									<div class="portfolio-detail">
										<div class="portfolio-overlay"></div>
										<a href="{{url('assets/perfect/img/demo-images/portfolio/full/08.jpg')}}" data-litebox-group="myGallery" class="litebox"></a>
									</div>
								</div>
							</div><!--portfolio col end-->
						
							<div class="col-sm-4 col-xs-12 pitem wordpress">
								<div class="portfolio_item">
									<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/portfolio/thumb/09.jpg')}}" alt="" />
									<div class="portfolio-detail">
										<div class="portfolio-overlay"></div>
										<a href="{{url('assets/perfect/img/demo-images/portfolio/full/09.jpg')}}" data-litebox-group="myGallery" class="litebox"></a>
									</div>
								</div>
							</div><!--portfolio col end-->
						
							<div class="col-sm-4 col-xs-12 pitem seo">
								<div class="portfolio_item">
									<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/portfolio/thumb/10.jpg')}}" alt="" />
									<div class="portfolio-detail">
										<div class="portfolio-overlay"></div>
										<a href="{{url('assets/perfect/img/demo-images/portfolio/full/10.jpg')}}" data-litebox-group="myGallery" class="litebox"></a>
									</div>
								</div>
							</div><!--portfolio col end-->
						
							<div class="col-sm-4 col-xs-12 pitem design">
								<div class="portfolio_item">
									<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/portfolio/thumb/11.jpg')}}" alt="" />
									<div class="portfolio-detail">
										<div class="portfolio-overlay"></div>
										<a href="{{url('assets/perfect/img/demo-images/portfolio/full/11.jpg')}}" data-litebox-group="myGallery" class="litebox"></a>
									</div>
								</div>
							</div><!--portfolio col end-->
						
							<div class="col-sm-4 col-xs-12 pitem seo">
								<div class="portfolio_item">
									<img class="img-responsive" src="{{url('assets/perfect/img/demo-images/portfolio/thumb/12.jpg')}}" alt="" />
									<div class="portfolio-detail">
										<div class="portfolio-overlay"></div>
										<a href="img/demo-images/portfolio/full/12.jpg" data-litebox-group="myGallery" class="litebox"></a>
									</div>
								</div>
							</div><!--portfolio col end-->
                        </div><!--#grid for filter items-->
                    </div><!--.col-md-12 of portfolio filter-->
                </div><!--.row-->
            </div>
        </section><!--work-->	