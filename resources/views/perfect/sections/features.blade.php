		<section id="features" class="section-padding">
			<div data-velocity="-.1" class="features-bg"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-6 wow animated fadeInLeft"  data-wow-duration="700ms" data-wow-delay="100ms">
						<div class="features-item">
							<i class="fa fa-arrows"></i>
							<h2>Responsive Design</h2>
							<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p>
						</div>
					</div><!--feature col end-->
					<div class="col-md-4 col-sm-6 wow animated fadeInLeft"  data-wow-duration="700ms" data-wow-delay="200ms">
						<div class="features-item">
							<i class="fa fa-eye"></i>
							<h2>Retina Display Ready</h2>
							<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p>
						</div>
					</div><!--feature col end-->
					<div class="col-md-4 col-sm-6 wow animated fadeInLeft"  data-wow-duration="700ms" data-wow-delay="300ms">
						<div class="features-item">
							<i class="fa fa-magic"></i>
							<h2>Parallax Style</h2>
							<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p>
						</div>
					</div><!--feature col end-->
					<div class="col-md-4 col-sm-6 wow animated fadeInLeft"  data-wow-duration="700ms" data-wow-delay="400ms">
						<div class="features-item">
							<i class="fa fa-adjust"></i>
							<h2>Freedom of color</h2>
							<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p>
						</div>
					</div><!--feature col end-->
					<div class="col-md-4 col-sm-6 wow animated fadeInLeft"  data-wow-duration="700ms" data-wow-delay="500ms">
						<div class="features-item">
							<i class="fa fa-tasks"></i>
							<h2>Build your own layout</h2>
							<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p>
						</div>
					</div><!--feature col end-->
					<div class="col-md-4 col-sm-6 wow animated fadeInLeft"  data-wow-duration="700ms" data-wow-delay="600ms">
						<div class="features-item">
							<i class="fa fa-rocket"></i>
							<h2>Lightweight & Super fast</h2>
							<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p>
						</div>
					</div><!--feature col end-->
					<div class="col-md-4 col-sm-6 wow animated fadeInLeft"  data-wow-duration="700ms" data-wow-delay="700ms">
						<div class="features-item">
							<i class="fa fa-code"></i>
							<h2>No code required</h2>
							<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p>
						</div>
					</div><!--feature col end-->
					<div class="col-md-4 col-sm-6 wow animated fadeInLeft"  data-wow-duration="700ms" data-wow-delay="800ms">
						<div class="features-item">
							<i class="fa fa-umbrella"></i>
							<h2>Cross-browser compatible</h2>
							<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p>
						</div>
					</div><!--feature col end-->
					<div class="col-md-4 col-sm-6 wow animated fadeInLeft"  data-wow-duration="700ms" data-wow-delay="900ms">
						<div class="features-item">
							<i class="fa fa-filter"></i>
							<h2>SEO freindly</h2>
							<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p>
						</div>
					</div><!--feature col end-->
				</div>
			</div>
		</section>