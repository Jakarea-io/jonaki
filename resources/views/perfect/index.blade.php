<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="utf-8">
		<title>Perfect Design | One page HTML5 template</title>
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="Perfect Point Marketing">
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Color Pickr -->
        <link href="{{url('assets/perfect/includes/color-pickr/color-pickr.css') }}" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="{{url('assets/perfect/includes/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
		<!-- WOW JS Animation -->
		<link rel="stylesheet" href="{{url('assets/perfect/includes/wow-js/animations.css') }}" />
		<!-- Litebox  -->
		<link rel="stylesheet" href="{{url('assets/perfect/includes/litebox/litebox.css') }}" />
		<!-- Owl Carousel -->
		<link rel="stylesheet" href="{{url('assets/perfect/includes/owl-carousel/owl.carousel.css') }}" />
        <!-- custom css -->
        <link href="{{url('assets/perfect/css/style.css') }}" rel="stylesheet" type="text/css" media="screen">
        <link href="{{url('assets/perfect/css/media.css') }}" rel="stylesheet" type="text/css" media="screen">
        <!-- font awesome for icons -->
        <link href="{{url('assets/perfect/includes/font-awesome-4.2.0/css/font-awesome.min.css') }}" rel="stylesheet">
		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
		<!-- Favicons -->
		<link rel="apple-touch-icon-precomposed" href="/apple-touch-icon-precomposed.png">
		<link rel="icon" href="/favicon.ico">

		<!--[if lte IE 8]>
			<link rel="stylesheet" type="text/css" href="css/fucking-ie-8-and-lower.css" />
		<![endif]-->
		
		
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body onload="init()" id="body">

		<div id="preloader">
			<div class="spinner">
				<div class="cube"></div>
				<div class="cube"></div>
				<div class="cube"></div>
				<div class="cube"></div>
				<div class="cube"></div>
				<div class="cube"></div>
				<div class="cube"></div>
				<div class="cube"></div>
				<div class="cube"></div>
			</div>			
		</div><!--preloader end-->
	

		@include('perfect.sections.slider');		
		
		
        <nav id="navigation">
            <div class="navbar navbar-default navbar-static-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.html">Perfect <span>Design</span></a>
                    </div>
                    <div class="navbar-collapse collapse">

                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#home">Home</a></li>
                            <li><a href="#about">About</a></li>
                            <li><a href="#features">Features</a></li>
                            <li><a href="#portfolio">Portfolio</a></li>
                            <li><a href="#testimonials">Testimonials</a></li>
                            <li><a href="#our-team">Team</a></li>
                            <li><a href="#pricing">Pricing</a></li>
                            <li><a href="#blog">Blog</a></li>
                            <li><a href="#contact">Contact</a></li>
                        </ul>

                    </div><!--/.nav-collapse -->
                </div><!--/.container -->
            </div><!--navbar-default-->
        </nav><!--navigation section end here-->
		
		@include('perfect.sections.about');		
		@include('perfect.sections.features');			
		@include('perfect.sections.portfolio');		
		@include('perfect.sections.testimonial');
		@include('perfect.sections.parallax');
		@include('perfect.sections.our-team');		
		@include('perfect.sections.pricing');		
		@include('perfect.sections.counter');		
		@include('perfect.sections.partners');		
		@include('perfect.sections.call-to-actions');		
		@include('perfect.sections.blogs');
		@include('perfect.sections.contact');		
		@include('perfect.sections.map');	
		

		
		<footer id="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-sm-9">
						<div class="footer-copyright">
							<p>Copyright 2013 <a href="">Perfect Design</a> | All Rights Reserved | Powered by HTML, CSS &amp; jQuery</p>
						</div>
					</div><!--copyright col end-->
					
					<div class="col-md-3 col-sm-3">
						<div class="company-name">
							<p>Perfect <span>Design</span></p>
						</div>
					</div><!--blogname col end-->
				</div>
			</div>
		</footer>

        <!--scripts and plugins -->
        <!--must need plugin jquery-->
        <script src="{{ url('assets/perfect/js/jquery.min.js')}}"></script>        
        <!--bootstrap js plugin-->
        <script src="{{ url('assets/perfect/includes/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
		<!-- Google Maps API -->
		<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<!-- Custom Google Map -->
		<script type="text/javascript">
			var maps;
			var geocoder;

			function init() {
				maps = [];
				geocoder = new google.maps.Geocoder();

				var styles = {
				  'Red': [
					{
					  featureType: 'all',
					  stylers: [{saturation:-100},{gamma:0.50}]
					}
				  ]
				};

				for (var s in styles) {
				  var opt = {
					mapTypeControlOptions: {
					  mapTypeIds: [s]
					},
					disableDefaultUI: true,
					navigationControl: true,
					scrollwheel: false,
					center: new google.maps.LatLng(42.415109, -96.842471),
					zoom: 8,
					mapTypeId: s
				  };

				  var div = document.getElementById('map');
				  var map = new google.maps.Map(div, opt);
				  var styledMapType = new google.maps.StyledMapType(styles[s], {name: s});
				  map.mapTypes.set(s, styledMapType);
				  
				  var goldStar = 'img/marker.png';

				  var marker = new google.maps.Marker({
					position: map.getCenter(),
					icon: goldStar,
					map: map
				  });				  
				}
			}
		</script>		
		<!--easying scroll effect-->
		<script src="{{ url('assets/perfect/js/jquery.easing.1.3.min.js')}}" type="text/javascript"></script>
		<script src="{{ url('assets/perfect/js/jquery.sticky.js')}}" type="text/javascript"></script>
		<script src="{{ url('assets/perfect/js/jquery.nav.js')}}" type="text/javascript"></script>
		<!--owl carousel-->
		<script src="{{ url('assets/perfect/includes/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script>
		<!--wow animation-->
		<script src="{{ url('assets/perfect/includes/wow-js/wow.min.js')}}" type="text/javascript"></script>
		
		<!--Activating WOW Animation only for modern browser-->
		<!--[if !IE]><!-->
			<script type="text/javascript">new WOW().init();</script>
		<!--<![endif]-->		
		
		<!--Oh Yes, IE 9+ Supports animation, lets activate for IE 9+-->
		<!--[if gte IE 9]>
			<script type="text/javascript">new WOW().init();</script>
		<![endif]-->		 
		
		<!--Opacity & Other IE fix for older browser-->
		<!--[if lte IE 8]>
			<script type="text/javascript" src="js/ie-opacity-polyfill.js"></script>
		<![endif]-->

		
		<!--parallax-->
		<script src="{{ url('assets/perfect/js/jquery.scrolly.js')}}" type="text/javascript"></script>
		<!--portfolio-filter-->
		<script src="{{ url('assets/perfect/js/isotope.pkgd.min.js')}}" type="text/javascript"></script>
		<!--lightbox-->
		<script src="{{ url('assets/perfect/includes/litebox/images-loaded.min.js')}}" type="text/javascript"></script>
		<script src="{{ url('assets/perfect/includes/litebox/litebox.min.js')}}" type="text/javascript"></script>
		<!--project count-->
		<script src="{{ url('assets/perfect/http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js')}}"></script>
		<script src="{{ url('assets/perfect/js/jquery.counterup.min.js')}}" type="text/javascript"></script>
		<!--custom scrollbar-->
		<script src="{{ url('assets/perfect/js/jquery.nicescroll.min.js')}}" type="text/javascript"></script>
		<!--Contact Form-->
		<script src="{{ url('assets/perfect/includes/contact-form/app.js')}}" type="text/javascript"></script>
		<script src="{{ url('assets/perfect/includes/contact-form/validatr.js')}}" type="text/javascript"></script>
		
		<!--customizable plugin edit according to your needs-->
		<script src="{{ url('assets/perfect/js/custom.js')}}" type="text/javascript"></script>
    </body>
</html>
