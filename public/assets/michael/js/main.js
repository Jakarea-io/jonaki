(function($) {
	$('.navbar-toggle').on('click', function(){
		$('#mobile-nav').slideToggle(300);
	});
	
    if($('#slider').length){
        $('#slider').nivoSlider({
            controlNavThumbs: true
        });
    }	
    if($('.owl-carousel').length){
        $('.owl-carousel').owlCarousel({
            items: 1,
            loop: true,
            autoplay:false,
            nav : true,
            dots: false,
            center: true
        });
    }
    if($('.testmonialslider').length){
      $('.testmonialslider').slick({
        autoplay: true
      });
    }

    if($('.author-slider').length){
      $('.author-slider').slick({
        autoplay: true,
        autoplaySpeed: 10000,
        fade: true
      });
    }

    new WOW().init();

})(jQuery);