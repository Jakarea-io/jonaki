<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',function(){
  return view('michael/landing');
});

Route::get('/blog',function(){
  return view('michael/blog');
});
Route::get('/event',function(){
  return view('michael/events_page');
});
Route::get('/keynote',function(){
  return view('michael/keynote');
});

Route::get('/consulting',function(){
  return view('michael/personal-consulting');
});

Route::get('data','ContactController@index');
Route::get('data/delete/{id}','ContactController@destroy');
Route::get('data/edit/{id}','ContactController@edit');
Route::post('data/edit/{id}','ContactController@update');
Route::get('/data-collection','ContactController@create');
Route::post('data-store','ContactController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
