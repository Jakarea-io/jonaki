<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datum extends Model
{
    protected $primaryKey = 'data_id';
    protected $fillable = ['first_name','last_name','phone','region','email','postal'];
}
