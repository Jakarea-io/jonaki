<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Datum;
class contactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $data = Datum::orderBy('data_id','desc')->get();
       return  view('michael/data',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('michael/contact-page');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
        'first_name.required'    => 'This is first name message',
        'first_name.max'    => 'This is first name message for max value',
        ];

        $request->validate([
            'first_name' => 'required|max:5',
            'last_name' => 'required|min:5|different:first_name',
            //'phone' => 'required|digits_between:1,5',
            //'phone' => 'required|between:1,5',
            //'phone' => 'date|after:tomorrow',
            'email' =>'required|email|max:50',
            'phone' => 'email|confirmed',
            'postal' =>'required|numeric|size:5',
            'ck' =>'accepted'

        ],$messages);

  

        $data = $request->all();
        Datum::create($data);
        return redirect('data')->with('message','data Successfully added');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Datum::find($id);
        return view('michael/edit')->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Datum::find($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->region = $request->region;
        $user->postal = $request->postal;
        $user->save();
        return redirect('data')->with('response', 'Data Updated Successfully');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Datum::find($id);
        if($user->delete()){
            return redirect('data');
        }
    }
}
